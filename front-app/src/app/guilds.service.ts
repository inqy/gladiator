import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GuildsService {

    private POST_REQUEST_WAR = '/api/guild/attack';
    private GET_WARS = '/api/guild/wars';
    private GET_WAR_DETAILS = '/api/guildwars/'

    constructor(private http: HttpClient) {
    }

    requestWar(target: number, success, failure): void {
        this.http.post(this.POST_REQUEST_WAR, { 'id': target }).subscribe(
            nothing => success && success(),
                err => failure && failure()
        );
    }

    getWars(success, failure): void {
        this.http.get(this.GET_WARS).subscribe(
            wars => success && success(wars),
            err => failure && failure()
        );
    }

    getWarDetails(id: number, success, failure): void {
        this.http.get(this.GET_WAR_DETAILS + id).subscribe(
            details => success && success(details),
            err => failure && failure()
        );
    }
}
