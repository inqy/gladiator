import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';
import {CharacterService} from './character.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {
    title = 'Gladiator';

    constructor(private http: HttpClient, private router: Router, private auth: AuthService, private character: CharacterService) {
    }

    logout() {
        this.auth.logout(() => {
            this.character.clean();
            this.router.navigateByUrl('/login');
        });
    }

}
