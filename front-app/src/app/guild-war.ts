export class GuildWar {
    id: number;
    attackerId: number;
    attackerName: string;
    defenderId: number;
    defenderName: string;
    result: boolean;
    finished: boolean;
    numberOfBattles: number;
}
