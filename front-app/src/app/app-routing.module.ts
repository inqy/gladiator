import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {CharacterFormComponent} from './character-form/character-form.component';
import {ProfileComponent} from './profile/profile.component';
import {BattleComponent} from './battle/battle.component';
import {GuildsComponent} from './guilds/guilds.component';
import {GuildWarComponent} from './guild-war/guild-war.component';
import {RankingsComponent} from './rankings/rankings.component';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'home'},
    {path: 'home', component: HomeComponent},
    {path: 'login', component: LoginComponent},
    {path: 'character', component: ProfileComponent},
    {path: 'guild', component: GuildsComponent},
    {path: 'battle', component: BattleComponent},
    {path: 'guildwar/:id', component: GuildWarComponent},
    {path: 'rankings', component: RankingsComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
