export class BattleResult {
    result: boolean;
    attacker: number;
    defender: number;
    xp: number;
    events: string[];
}
