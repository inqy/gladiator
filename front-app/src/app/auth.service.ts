import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    authenticated: boolean;
    private AUTH = '/api/user';
    private LOGOUT = '/api/logout';


    constructor(private http: HttpClient) {
        this.http.get(this.AUTH).subscribe(response => {
            this.authenticated = response['name'];
        });
    }

    authenticate(credentials, callback) {
        const headers = new HttpHeaders(credentials ? {
            authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
        } : {});

        this.http.get(this.AUTH, {headers: headers}).subscribe(response => {
            this.authenticated = response['name'];
            return callback && callback();
        });
    }

    logout(callback) {
        this.http.post(this.LOGOUT, {}).subscribe(() => {
            this.authenticated = false;
            return callback && callback();
        });
    }
}
