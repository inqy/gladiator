import {Component, OnInit} from '@angular/core';
import {CharacterService} from '../character.service';
import {RequestStatus} from '../enum/request-status.enum';

@Component({
    selector: 'app-character-form',
    templateUrl: './character-form.component.html',
    styleUrls: ['./character-form.component.less']
})
export class CharacterFormComponent implements OnInit {

    characterName: string;
    status: RequestStatus = RequestStatus.UNSUBMITTED;
    characterCreated: number;

    constructor(private service: CharacterService) { }

    onSubmit() {
        this.status = RequestStatus.SUBMITTED;
        this.service.createCharacter(this.characterName,
            (value) => {
                this.characterCreated = value;
                this.status = RequestStatus.SUCCESS;
                this.service.getCharacter(() => {}, () => {});
            },
            (err) => { this.status = RequestStatus.FAIL; });
    }

    ngOnInit() {
        this.status = RequestStatus.UNSUBMITTED;
        this.characterCreated = null;
    }

    isUnsubmitted(): boolean {
        return this.status === RequestStatus.UNSUBMITTED;
    }
}
