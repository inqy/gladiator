import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Character} from './character';
import {RequestStatus} from './enum/request-status.enum';

@Injectable({
    providedIn: 'root'
})
export class CharacterService implements OnInit {

    public hasCharacter = false;
    public character: Character;
    private GET_CHARACTERS = '/api/characters';
    private CREATE_CHARACTER = '/api/characters';
    private GET_CHARACTER = '/api/character';

    constructor(private http: HttpClient) {
    }

    getCharacters(): Observable<Character[]> {
        return this.http.get<Character[]>(this.GET_CHARACTERS);
    }

    getCharacter(success, failure) {
        return this.http.get<Character>(this.GET_CHARACTER).subscribe(
            (value) => {
                    this.hasCharacter = true;
                    this.character = value;
                    return success && success();
                },
            (error) => {
                    this.hasCharacter = false;
                    return failure && failure();
                }
        );
    }

    createCharacter(characterName: string, success, failure): void {
        const payload = { 'name': characterName };
        this.http.post<number>(this.CREATE_CHARACTER, payload).subscribe(
            value => { this.getCharacter(() => {}, () => {}); return success && success(); },
            err => failure && failure()
        );
    }

    ngOnInit(): void {
        // this.getCharacter(() => {}, () => {});
    }

    clean(): void {
        this.hasCharacter = false;
        this.character = null;
    }
}

