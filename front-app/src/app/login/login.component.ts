import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent {


    credentials = {username: '', password: ''};

    constructor(private auth: AuthService, private http: HttpClient, private router: Router) { }

    login() {
        this.auth.authenticate(this.credentials, () => {
            this.router.navigateByUrl('/');
        });
        return false;
    }

}
