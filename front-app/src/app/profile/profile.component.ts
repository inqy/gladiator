import {Component, OnInit} from '@angular/core';
import {Character} from '../character';
import {CharacterService} from '../character.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
    constructor(private characterProvider: CharacterService) {
    }

    ngOnInit() {
        this.characterProvider.getCharacter(() => {}, () => {});
    }
}
