import {Component, OnInit} from '@angular/core';
import {BattleService} from '../battle.service';
import {Rating} from '../rating';

@Component({
    selector: 'app-rankings',
    templateUrl: './rankings.component.html',
    styleUrls: ['./rankings.component.less']
})
export class RankingsComponent implements OnInit {

    private rankings: Rating[];
    constructor(private service: BattleService) {
    }

    ngOnInit() {
        this.service.getRankings((rankings) => { this.rankings = rankings; }, (err) => {});
    }

}
