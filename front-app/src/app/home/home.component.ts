import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth.service';

class AppService {
}

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less']
})
export class HomeComponent {
    greeting = {};

    constructor(private auth: AuthService, private http: HttpClient) {
        http.get('/api/characters').subscribe(data => this.greeting = data);
    }

    authenticated() {
        return this.auth.authenticated;
    }
}
