import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GuildWarDetails} from '../guild-war-details';
import {GuildsService} from '../guilds.service';

@Component({
    selector: 'app-guild-war',
    templateUrl: './guild-war.component.html',
    styleUrls: ['./guild-war.component.less']
})
export class GuildWarComponent implements OnInit {

    private id: string;
    private loading: boolean;
    private details: GuildWarDetails;

    constructor(private route: ActivatedRoute, private service: GuildsService) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.loading = true;
        this.service.getWarDetails(Number(this.id), (details) => { this.details = details; this.loading = false; }, (err) => {});
    }
}
