export class Rating {
    characterId: number;
    name: string;
    score: number;
}
