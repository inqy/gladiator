import {Component, OnInit} from '@angular/core';
import {GuildsService} from '../guilds.service';
import {GuildWar} from '../guild-war';
import {CharacterService} from '../character.service';

@Component({
    selector: 'app-guilds',
    templateUrl: './guilds.component.html',
    styleUrls: ['./guilds.component.less']
})
export class GuildsComponent implements OnInit {

    private message: string;
    private guildId: number;
    private isSending = false;
    private guildWars: GuildWar[] = Array();

    constructor(private service: GuildsService, private characterService: CharacterService) {
    }

    ngOnInit() {
        this.service.getWars((wars) => {
            if (wars !== undefined && wars.length > 0) {
                this.guildWars = wars;
            }
        }, () => {});
    }

    onSubmit(): void {
        this.isSending = true;
        this.service.requestWar(this.guildId,
        () => {
            this.isSending = false;
            this.message = `Requested war with guild ${this.guildId}.`;
        },
        () => {
            this.isSending = false;
            this.message = `Request failed`;
        });
    }

    statusMessage(war: GuildWar): string {
        const myGuildId = this.characterService.character.guildId;
        if (!war.finished) {
            return 'Ongoing';
        }
        if ((war.result && war.attackerId === myGuildId) || (!war.result && war.defenderId === myGuildId)) {
            return 'Won';
        } else {
            return 'Lost';
        }
    }
}
