import {Component, OnInit} from '@angular/core';
import {CharacterService} from '../character.service';
import {Character} from '../character';
import {BattleService} from '../battle.service';
import {Router} from '@angular/router';
import {BattleResult} from '../battle-result';

@Component({
    selector: 'app-battle',
    templateUrl: './battle.component.html',
    styleUrls: ['./battle.component.less']
})
export class BattleComponent implements OnInit {

    potentialTargets: Character[];
    loading = false;
    battleResultPresent = false;
    battleResult: BattleResult;

    constructor(private characterService: CharacterService, private battleService: BattleService, private router: Router) {
    }

    getPotentialTargets() {
        this.characterService.getCharacters().subscribe(
            value => {
                this.potentialTargets = value.filter(
                    (val: Character, index: number, array: Character[]) => val.id !== this.characterService.character.id);
            }
        );
    }

    ngOnInit() {
        this.getPotentialTargets();
    }

    attack(target: Character) {
        this.loading = true;
        this.battleService.attack(target, (val) => {
            this.loading = false;
            this.battleResultPresent = true;
            this.battleResult = val;
        }, {});
    }
}
