export enum RequestStatus {
    UNSUBMITTED,
    SUBMITTED,
    SUCCESS,
    FAIL
}

