import {GuildWar} from './guild-war';
import {BattleResult} from './battle-result';

export class GuildWarDetails {
    info: GuildWar;
    battles: BattleResult[];
}
