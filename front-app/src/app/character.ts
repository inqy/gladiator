export class Character {
    id: number;
    name: string;
    guildId: number;
    guildName: string;
    level: number;
    hp: number;
    exp: number;
    strength: number;
    agility: number;
    endurance: number;
    speed: number;
}
