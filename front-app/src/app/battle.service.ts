import {Injectable} from '@angular/core';
import {Character} from './character';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class BattleService {

    private ATTACK = '/api/attack';
    private RANKINGS = '/api/ratings';

    constructor(private http: HttpClient) {
    }

    attack(target: Character, success, failure) {
        this.http.post(this.ATTACK, target.id).subscribe(
            value => success && success(value),
            error => failure && failure(error)
        );
    }

    getRankings(success, failure) {
        this.http.get(this.RANKINGS).subscribe(
            value => success && success(value),
            error => failure && failure(error)
        );
    }
}
