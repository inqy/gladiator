create sequence hibernate_sequence start 1 increment 1;

create table authority
(
    id        int8 not null,
    authority varchar(255),
    primary key (id)
);

create table battle
(
    id          int8 not null,
    result      boolean,
    attacker_id int8,
    defender_id int8,
    primary key (id)
);

create table battle_event
(
    battle_id   int8 not null,
    event_id    int4 not null,
    attacker_hp int8,
    defender_hp int8,
    message     varchar(255),
    primary key (battle_id, event_id)
);

create table character
(
    id               int8 not null,
    agility          int8,
    endurance        int8,
    exp              int8,
    hp               int8,
    level            int8,
    name             varchar(255),
    speed            int8,
    strength         int8,
    guild_id         int8,
    security_user_id int8,
    primary key (id)
);

create table guild
(
    id   int8 not null,
    name varchar(255),
    primary key (id)
);

create table guild_war
(
    id          int8 not null,
    finished    boolean,
    result      boolean,
    attacker_id int8,
    defender_id int8,
    primary key (id)
);

create table guild_war_battle
(
    guild_war_id int8 not null,
    battle_id    int8 not null
);

create table security_user
(
    id                      int8    not null,
    account_non_expired     boolean not null,
    account_non_locked      boolean not null,
    credentials_non_expired boolean not null,
    enabled                 boolean not null,
    password                varchar(255),
    username                varchar(255),
    primary key (id)
);

create table security_user_authorities
(
    security_user_id int8 not null,
    authorities_id   int8 not null
);

alter table authority
    add constraint UK_nrgoi6sdvipfsloa7ykxwlslf unique (authority);

alter table guild_war_battle
    add constraint UK_k5gcjgq7ce7gk9a29wl8wma5a unique (battle_id);

alter table battle
    add constraint FKs6q5u3o0dtxx3ly1ket540sre
        foreign key (attacker_id)
            references character;

alter table battle
    add constraint FKaa2j2hxaxwq6c1myjomwvu67x
        foreign key (defender_id)
            references character;

alter table battle_event
    add constraint FK5th7syiikblf0mc5ycajvqqqy
        foreign key (battle_id)
            references battle;

alter table character
    add constraint FKbun3ftlhd0wdldrvv38m8w3ky
        foreign key (guild_id)
            references guild;

alter table character
    add constraint FK2bjthribhf3tcg4knf0twm64n
        foreign key (security_user_id)
            references security_user;

alter table guild_war
    add constraint FKjdfb4n9pv6673aj4s4rllbkce
        foreign key (attacker_id)
            references guild;

alter table guild_war
    add constraint FKtmaoux8g8uf4iwshql7oxqbyx
        foreign key (defender_id)
            references guild;

alter table guild_war_battle
    add constraint FKaiyapp6oirpgr3ct2ds46kwm4
        foreign key (battle_id)
            references battle;

alter table guild_war_battle
    add constraint FKlpsmdxx0spmfsmhl1bcxxh9xb
        foreign key (guild_war_id)
            references guild_war;

alter table security_user_authorities
    add constraint FK4ai4a0oq4fcv8crcjn95j5mag
        foreign key (authorities_id)
            references authority;

alter table security_user_authorities
    add constraint FKnao35tilxmmrt4jjdn15p74tn
        foreign key (security_user_id)
            references security_user;
