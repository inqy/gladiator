package io.inqy.gladiator.demo.controller;

import io.inqy.gladiator.demo.service.RatingService;
import io.inqy.gladiator.demo.transit.RatingResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RatingController {
    private final RatingService service;

    @GetMapping("/ratings")
    List<RatingResponse> getRatings() {
        return service.getDailyRanking();
    }
}
