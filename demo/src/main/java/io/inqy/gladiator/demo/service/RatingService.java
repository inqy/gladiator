package io.inqy.gladiator.demo.service;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.exception.EntityNotFoundException;
import io.inqy.gladiator.demo.repository.CharacterRepository;
import io.inqy.gladiator.demo.service.util.TimeService;
import io.inqy.gladiator.demo.transit.RatingResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RatingService {
    private final TimeService timeProvider;
    private final CharacterRepository characterRepository;
    private final ZSetOperations<String, Long> operations;

    public List<RatingResponse> getDailyRanking() {
        return operations.reverseRangeWithScores(timeProvider.getTodayAsKey(), 0, 9).stream()
            .map(tuple -> RatingResponse.fromTuple(tuple, characterRepository.findById(tuple.getValue()).orElseThrow(EntityNotFoundException::new).getName()))
            .collect(Collectors.toList());
    }

    public void increaseCharacterDailyRating(Character character) {
        operations.incrementScore(timeProvider.getTodayAsKey(), character.getId(), 1.0);
    }

    public void increaseCharacterDailyRating(Character character, double value) {
        operations.incrementScore(timeProvider.getTodayAsKey(), character.getId(), value);
    }
}
