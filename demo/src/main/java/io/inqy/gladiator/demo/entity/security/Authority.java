package io.inqy.gladiator.demo.entity.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Authority implements GrantedAuthority {

    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true)
    private String authority;
}
