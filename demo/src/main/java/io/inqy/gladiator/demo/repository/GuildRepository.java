package io.inqy.gladiator.demo.repository;

import io.inqy.gladiator.demo.entity.Guild;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuildRepository extends JpaRepository<Guild, Long> {
}
