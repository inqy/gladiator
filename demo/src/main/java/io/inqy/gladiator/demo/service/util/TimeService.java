package io.inqy.gladiator.demo.service.util;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Service
public class TimeService {

    private DateTimeFormatter todayKeyFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"));
    private DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern( "uuuu.MM.dd.HH.mm.ss" ).withZone(ZoneId.of("UTC"));

    public String getTodayAsKey() {
        LocalDate today = LocalDate.now(ZoneId.of("UTC"));
        return todayKeyFormatter.format(today);
    }

    public String getTimestamp() {
        return LocalDateTime.now(ZoneId.of("UTC")).format(timestampFormatter);
    }

    public LocalDate getTimeFromTimestamp(String timestamp) {
        return LocalDate.from(timestampFormatter.parse(timestamp));
    }
}
