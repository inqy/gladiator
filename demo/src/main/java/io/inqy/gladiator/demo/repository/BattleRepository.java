package io.inqy.gladiator.demo.repository;

import io.inqy.gladiator.demo.entity.Battle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleRepository extends JpaRepository<Battle, Long> {
}
