package io.inqy.gladiator.demo;

import io.inqy.gladiator.demo.repository.security.SecurityUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

@SpringBootApplication
public class DemoApplication {

    @Bean
    ZSetOperations<String, Long> zSetsOps(@Qualifier("redisTemplate") RedisTemplate template) {
        return template.opsForZSet();
    }

    @RequiredArgsConstructor
    @Configuration
    @Order(SecurityProperties.DEFAULT_FILTER_ORDER)
    protected static class SecurityConfiguration extends WebSecurityConfigurerAdapter {

        private final SecurityUserRepository userRepository;
        private final UserDetailsService userDetailsService;

        @Override
        protected void configure(final AuthenticationManagerBuilder auth) {
            auth.authenticationProvider(authenticationProvider());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .successForwardUrl("/home")
                    .permitAll()
                    .and()
                .httpBasic()
                    .and()
                .csrf()
                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        }

        @Bean
        public DaoAuthenticationProvider authenticationProvider() {
            DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
            authProvider.setUserDetailsService(userDetailsService);
            authProvider.setPasswordEncoder(encoder());
            return authProvider;
        }

        @Bean
        public PasswordEncoder encoder() {
            return new BCryptPasswordEncoder(11);
        }
    }

    @Configuration
    @EnableRedisHttpSession
    public class Config {

    }

    public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
        public SecurityInitializer() {
            super(SecurityConfig.class, Config.class);
        }
    }

    public class Initializer extends AbstractHttpSessionApplicationInitializer {

    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
