package io.inqy.gladiator.demo.queue;

import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.exception.EntityNotFoundException;
import io.inqy.gladiator.demo.queue.message.GuildWarMessage;
import io.inqy.gladiator.demo.repository.GuildRepository;
import io.inqy.gladiator.demo.service.BattleService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GuildWarReceiver {

    private final GuildRepository guildRepository;
    private final BattleService battleService;

    @RabbitListener(queues = QueueSetup.queueName, containerFactory = "demoFactory")
    public void receiveMessage(GuildWarMessage message) {
        try {
            Guild attacker = guildRepository.findById(message.getAttacker()).orElseThrow(EntityNotFoundException::new);
            Guild defender = guildRepository.findById(message.getDefender()).orElseThrow(EntityNotFoundException::new);

            battleService.guildWar(attacker, defender);
        } catch (EntityNotFoundException e) {}
    }
}
