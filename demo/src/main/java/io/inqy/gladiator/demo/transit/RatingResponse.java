package io.inqy.gladiator.demo.transit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.ZSetOperations;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RatingResponse {
    private Long characterId;
    private String name;
    private Double score;

    public static RatingResponse fromTuple(ZSetOperations.TypedTuple<Long> tuple, String name) {
        return RatingResponse.builder()
            .characterId(tuple.getValue())
            .name(name)
            .score(tuple.getScore())
            .build();
    }
}
