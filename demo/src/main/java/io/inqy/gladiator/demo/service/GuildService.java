package io.inqy.gladiator.demo.service;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.entity.GuildWar;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.exception.EntityNotFoundException;
import io.inqy.gladiator.demo.queue.GuildWarSender;
import io.inqy.gladiator.demo.repository.CharacterRepository;
import io.inqy.gladiator.demo.repository.GuildRepository;
import io.inqy.gladiator.demo.repository.GuildWarRepository;
import io.inqy.gladiator.demo.transit.GetGuildWarDetailsResponse;
import io.inqy.gladiator.demo.transit.GetGuildWarResponse;
import io.inqy.gladiator.demo.transit.GuildResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GuildService {

    private final GuildWarSender guildWarSender;
    private final GuildRepository guildRepository;
    private final CharacterRepository characterRepository;
    private final GuildWarRepository guildWarRepository;

    public GuildResponse getGuild(Long id) {
        Guild guild = guildRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return new GuildResponse(guild.getId(), guild.getName());
    }

    public void requestGuildWar(SecurityUser user, Long targetGuild) {
        Character attacker = characterRepository.findCharacterByUser(user).orElseThrow(EntityNotFoundException::new);
        Guild attackerGuild = Optional.of(attacker.getGuild()).orElseThrow(EntityNotFoundException::new);
        Guild defenderGuild = guildRepository.findById(targetGuild).orElseThrow(EntityNotFoundException::new);
        guildWarSender.requestWar(attackerGuild.getId(), defenderGuild.getId());
    }

    public List<GetGuildWarResponse> getGuildWars(SecurityUser user) {
        Character character = characterRepository.findCharacterByUser(user).orElseThrow(EntityNotFoundException::new);
        Guild guild = Optional.of(character.getGuild()).orElseThrow(EntityNotFoundException::new);
        return getGuildWarsOfGuild(guild);
    }

    private List<GetGuildWarResponse> getGuildWarsOfGuild(Guild guild) {
        List<GuildWar> wars = guildWarRepository.findAllByAttackerEqualsOrDefenderEquals(guild, guild);
        return wars.stream().map(GetGuildWarResponse::fromEntity).collect(Collectors.toList());
    }

    public GetGuildWarDetailsResponse getGuildWarDetails(Long id) {
        GuildWar war = guildWarRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return GetGuildWarDetailsResponse.fromEntity(war);
    }
}
