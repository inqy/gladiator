package io.inqy.gladiator.demo.controller.security;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {

    @RequestMapping("/home")
    public String home() {
        return "Hello, world!";
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}
