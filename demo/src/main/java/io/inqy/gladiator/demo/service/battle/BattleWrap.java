package io.inqy.gladiator.demo.service.battle;

import io.inqy.gladiator.demo.entity.BattleEvent;
import io.inqy.gladiator.demo.entity.Character;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
public class BattleWrap {

    private Random random;
    private CharacterWrap attacker;
    private CharacterWrap defender;
    private Boolean result;
    private List<BattleEvent> events;

    public BattleWrap(Character attacker, Character defender, Random random) {
        this.attacker = new CharacterWrap(attacker);
        this.defender = new CharacterWrap(defender);
        this.random = random;
        this.result = null;
    }

    public void perform() {
        events = new ArrayList<>();
        int eventId = 0;
        while (attacker.getHp() > 0 && defender.getHp() > 0) {
            String message;
            if (attacker.getTurnPoints() >= defender.getTurnPoints()) {
                message = takeTurn(attacker, defender);
            } else {
                message = takeTurn(defender, attacker);
            }
            events.add(new BattleEvent(null, eventId++, null,  attacker.getHp(), defender.getHp(), message));
        }
        this.result = attacker.getHp() > 0;
    }

    public List<BattleEvent> getEvents() {
        if(result == null) {
            throw new IllegalStateException();
        }
        return events;
    }

    private String takeTurn(CharacterWrap active, CharacterWrap target) {
        String message;
        if (active.tryHit(random, target)) {
            long damage = active.getDamage(random);
            message = String.format("%s hits %s for %d points of damage", attacker.getCharacter().getName(), defender.getCharacter().getName(), damage);
            target.setHp(target.getHp() - damage);
        } else {
            message = String.format("%s swings his weapon, but misses", attacker.getCharacter().getName());
        }
        target.setTurnPoints(target.getTurnPoints() + target.getCharacter().getSpeed());
        return message;
    }
}
