package io.inqy.gladiator.demo.service.battle;

import io.inqy.gladiator.demo.entity.Character;
import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Getter
@Setter
public class CharacterWrap {

    private Character character;
    private long hp;
    private long turnPoints;
    private float hitChance;
    private long maxDamage;
    private long minDamage;

    CharacterWrap(Character character) {
        this.character = character;
        hp = character.getHp();
        turnPoints = character.getSpeed();
        maxDamage = character.getStrength() * 3 / 2;
        minDamage = character.getStrength() / 2;
    }

    Boolean tryHit(Random random, CharacterWrap target) {
        hitChance = (float) character.getAgility() / (character.getAgility() + target.getCharacter().getAgility());
        return random.nextFloat() < hitChance;
    }

    Long getDamage(Random random) {
        return random.nextInt((int) (maxDamage - minDamage)) + minDamage;
    }
}
