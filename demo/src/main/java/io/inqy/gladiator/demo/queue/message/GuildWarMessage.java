package io.inqy.gladiator.demo.queue.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GuildWarMessage {
    Long attacker;
    Long defender;
    String requested;
}
