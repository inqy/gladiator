package io.inqy.gladiator.demo.runner;

import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationRunner {

    private static final int N = 100;
    private final DataLoaderService dataLoaderService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        createDefaultUsers();

        Guild a = dataLoaderService.createGuild("Ferocious Warriors");
        Guild b = dataLoaderService.createGuild("Glorious Soldiers");

        IntStream.rangeClosed(0, N - 1).forEach( i -> {
            Guild guild = i % 2 == 0 ? a : b;
            dataLoaderService.createUserWithGuild(i, guild);
        });
    }

    private void createDefaultUsers() {
        dataLoaderService.createUser("hello", "kitty");
        dataLoaderService.createUser("welcome", "feline");
    }
}
