package io.inqy.gladiator.demo.transit;

import io.inqy.gladiator.demo.entity.Battle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BattleResultResponse {
    /**
     * True if the attacker has won
     */
    Boolean result;
    Long attacker;
    Long defender;
    Long xp;
    List<String> events;

    public static BattleResultResponse fromEntityAndEvents(Battle battle, List<String> events) {
        return BattleResultResponse.builder()
            .attacker(battle.getAttacker().getId())
            .defender(battle.getDefender().getId())
            .xp(10L)
            .result(battle.getResult())
            .events(events)
            .build();
    }
}
