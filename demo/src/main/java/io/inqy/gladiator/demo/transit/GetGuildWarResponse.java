package io.inqy.gladiator.demo.transit;

import io.inqy.gladiator.demo.entity.GuildWar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GetGuildWarResponse {

    Long id;
    Long attackerId;
    String attackerName;
    Long defenderId;
    String defenderName;
    Boolean result;
    Boolean finished;
    Long numberOfBattles;

    public static GetGuildWarResponse fromEntity(GuildWar guildWar) {
        return GetGuildWarResponse.builder()
            .id(guildWar.getId())
            .attackerId(guildWar.getAttacker().getId())
            .attackerName(guildWar.getAttacker().getName())
            .defenderId(guildWar.getDefender().getId())
            .defenderName(guildWar.getDefender().getName())
            .result(guildWar.getResult())
            .finished(guildWar.getFinished())
            .numberOfBattles((long) guildWar.getBattle().size())
            .build();
    }
}
