package io.inqy.gladiator.demo.entity;

import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.transit.CreateCharacterRequest;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@AllArgsConstructor
@Data
public class Character {
    public static final Long DEFAULT_LEVEL = 1L;
    public static final Long DEFAULT_MAXHP = 100L;
    public static final long DEFAULT_ENDURANCE = 10L;
    public static final long DEFAULT_AGILITY = 10L;
    public static final long DEFAULT_STRENGTH = 10L;
    public static final long DEFAULT_SPEED = 10L;
    public static final long DEFAULT_EXP = 0L;

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private SecurityUser securityUser;
    @ManyToOne
    private Guild guild;
    private String name;
    private Long level;
    private Long hp;
    private Long exp;
    private Long strength;
    private Long agility;
    private Long endurance;
    private Long speed;

    protected Character() {}

    public static Character createFromRequest(CreateCharacterRequest request, SecurityUser user) {
        return new Character(null, user, null, request.getName(), DEFAULT_LEVEL, DEFAULT_MAXHP,
            DEFAULT_EXP, DEFAULT_STRENGTH, DEFAULT_AGILITY, DEFAULT_ENDURANCE, DEFAULT_SPEED);
    }
}
