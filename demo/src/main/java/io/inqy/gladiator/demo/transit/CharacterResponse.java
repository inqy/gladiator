package io.inqy.gladiator.demo.transit;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.Guild;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CharacterResponse {
    private Long id;
    private String name;
    private Long guildId;
    private String guildName;
    private Long level;
    private Long hp;
    private Long exp;
    private Long strength;
    private Long agility;
    private Long endurance;
    private Long speed;

    public static CharacterResponse fromEntity(Character character) {
        Guild guild = character.getGuild();
        return CharacterResponse.builder()
            .id(character.getId())
            .name(character.getName())
            .guildId(guild != null? guild.getId() : null)
            .guildName(guild != null? guild.getName() : null)
            .level(character.getLevel())
            .hp(character.getHp())
            .exp(character.getExp())
            .strength(character.getStrength())
            .agility(character.getAgility())
            .endurance(character.getEndurance())
            .speed(character.getSpeed())
            .build();
    }
}
