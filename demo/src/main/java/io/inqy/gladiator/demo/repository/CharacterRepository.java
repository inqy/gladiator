package io.inqy.gladiator.demo.repository;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

public interface CharacterRepository extends JpaRepository<Character, Long> {

    List<Character> findCharactersByGuildEquals(@Param("guild") Guild guild);

    @Query("SELECT C FROM Character C where C.securityUser = :user")
    Optional<Character> findCharacterByUser(@Param("user") SecurityUser user);

    @Query("SELECT C FROM Character C where C.securityUser = :user")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Character> findCharacterByUserLocking(@Param("user") SecurityUser user);

    @Query("SELECT C FROM Character C where C.id = :id")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Character> findByIdLocking(@Param("id") Long id);
}
