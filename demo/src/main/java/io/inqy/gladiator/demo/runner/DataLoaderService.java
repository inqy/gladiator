package io.inqy.gladiator.demo.runner;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.repository.CharacterRepository;
import io.inqy.gladiator.demo.repository.GuildRepository;
import io.inqy.gladiator.demo.repository.security.SecurityUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class DataLoaderService {

    private final SecurityUserRepository userRepository;
    private final CharacterRepository characterRepository;
    private final PasswordEncoder encoder;
    private final GuildRepository guildRepository;

    @Transactional
    public void createUserWithGuild(int i, Guild guild) {
        SecurityUser user = createUser("user" + i, "password" + i);
        createCharacter(user, guild, "character" + i);
    }

    @Transactional
    public SecurityUser createUser(String username, String password) {
        SecurityUser user = SecurityUser.builder()
            .username(username)
            .password(encoder.encode(password))
            .accountNonExpired(true)
            .accountNonLocked(true)
            .credentialsNonExpired(true)
            .enabled(true)
            .build();
        userRepository.save(user);
        return user;
    }

    @Transactional
    public void createCharacter(SecurityUser user, Guild guild, String name) {
        Character character = new Character(null, user, guild, name,
            Character.DEFAULT_LEVEL, Character.DEFAULT_MAXHP, Character.DEFAULT_EXP, Character.DEFAULT_STRENGTH,
            Character.DEFAULT_AGILITY, Character.DEFAULT_ENDURANCE, Character.DEFAULT_SPEED);
        characterRepository.save(character);
    }

    @Transactional
    public Guild createGuild(String name) {
        Guild guild = new Guild(null, name);
        guildRepository.save(guild);
        return guild;
    }
}
