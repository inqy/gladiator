package io.inqy.gladiator.demo.transit;

import io.inqy.gladiator.demo.entity.GuildWar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class GetGuildWarDetailsResponse {

    GetGuildWarResponse info;
    List<BattleResultResponse> battles;

    public static GetGuildWarDetailsResponse fromEntity(GuildWar war) {
        return GetGuildWarDetailsResponse.builder()
            .info(GetGuildWarResponse.fromEntity(war))
            .battles(war.getBattle().stream()
                .map(battle -> BattleResultResponse.fromEntityAndEvents(battle, Collections.emptyList()))
                .collect(Collectors.toList()))
            .build();
    }
}
