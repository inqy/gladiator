package io.inqy.gladiator.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuildWar {
    @Id
    @GeneratedValue
    Long id;
    @OneToOne
    Guild attacker;
    @OneToOne
    Guild defender;
    Boolean result;
    Boolean finished;
    @OneToMany
    List<Battle> battle;
}
