package io.inqy.gladiator.demo.controller;

import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.service.GuildService;
import io.inqy.gladiator.demo.transit.GetGuildWarDetailsResponse;
import io.inqy.gladiator.demo.transit.GetGuildWarResponse;
import io.inqy.gladiator.demo.transit.GuildResponse;
import io.inqy.gladiator.demo.transit.GuildWarRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GuildController {

    private final GuildService guildService;

    @GetMapping("/guilds/{id}")
    GuildResponse getGuild(@PathVariable long id) {
        return guildService.getGuild(id);
    }

    @PostMapping("/guild/attack")
    void requestGuildWar(Authentication auth, @RequestBody GuildWarRequest request) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        guildService.requestGuildWar(user, request.getId());
    }

    @GetMapping("/guild/wars")
    List<GetGuildWarResponse> getGuildWars(Authentication auth) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        return guildService.getGuildWars(user);
    }

    @GetMapping("/guildwars/{id}")
    GetGuildWarDetailsResponse getGuildWarDetails(@PathVariable long id) {
        return guildService.getGuildWarDetails(id);
    }

}
