package io.inqy.gladiator.demo.queue;

import io.inqy.gladiator.demo.queue.message.GuildWarMessage;
import io.inqy.gladiator.demo.service.util.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GuildWarSender {
    private final RabbitTemplate rabbitTemplate;
    private final TimeService timeService;

    public void requestWar(Long attacker, Long defender) {
        GuildWarMessage request = new GuildWarMessage(attacker, defender, timeService.getTimestamp());
        rabbitTemplate.convertAndSend(QueueSetup.topicExchangeName, "demo.guildwar.attack", request);
    }
}
