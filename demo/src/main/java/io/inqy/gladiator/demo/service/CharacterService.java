package io.inqy.gladiator.demo.service;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.exception.CharacterAlreadyExistsException;
import io.inqy.gladiator.demo.exception.EntityNotFoundException;
import io.inqy.gladiator.demo.repository.CharacterRepository;
import io.inqy.gladiator.demo.repository.security.SecurityUserRepository;
import io.inqy.gladiator.demo.transit.CreateCharacterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CharacterService {

    private final SecurityUserRepository userRepository;
    private final CharacterRepository characterRepository;

    public Character get(SecurityUser user) {
        return characterRepository.findCharacterByUser(user).orElseThrow(EntityNotFoundException::new);
    }

    public Character get(Long id) {
        return characterRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public List<Character> getAll() {
        return characterRepository.findAll();
    }

    @Transactional
    public Long create(SecurityUser user, CreateCharacterRequest request) {
        user = userRepository.findUserByIdLocking(user.getId()).orElseThrow(RuntimeException::new);
        if (characterRepository.findCharacterByUser(user).isPresent()) {
            throw new CharacterAlreadyExistsException();
        }
        Character character = Character.createFromRequest(request, user);
        Character savedCharacter = characterRepository.save(character);
        return savedCharacter.getId();
    }

    @Transactional
    public void delete(SecurityUser user, Long characterId) {
        user = userRepository.findUserByIdLocking(user.getId()).orElseThrow(RuntimeException::new);
        Character character = characterRepository.findCharacterByUser(user).orElseThrow(EntityNotFoundException::new);
        if (!character.getId().equals(characterId)) {
            throw new RuntimeException();
        }
        characterRepository.delete(character);
    }

    @Transactional
    public void increaseCharacterExperience(Long characterId, Long experience) {
        Character character = characterRepository.findByIdLocking(characterId).orElseThrow(EntityNotFoundException::new);
        character.setExp(character.getExp() + experience);
        characterRepository.save(character);
    }
}
