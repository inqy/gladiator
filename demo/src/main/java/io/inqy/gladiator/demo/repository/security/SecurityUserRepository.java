package io.inqy.gladiator.demo.repository.security;

import io.inqy.gladiator.demo.entity.security.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface SecurityUserRepository extends JpaRepository<SecurityUser, Long> {
    @Query("SELECT U FROM SecurityUser U where U.username = :username")
    Optional<SecurityUser> findUserByUsername(@Param("username") String username);

    @Query("SELECT U FROM SecurityUser U where U.id = :id")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<SecurityUser> findUserByIdLocking(@Param("id") Long Id);
}
