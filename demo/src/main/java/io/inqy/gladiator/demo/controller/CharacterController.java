package io.inqy.gladiator.demo.controller;

import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.service.CharacterService;
import io.inqy.gladiator.demo.transit.CharacterResponse;
import io.inqy.gladiator.demo.transit.CreateCharacterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class CharacterController {

    private final CharacterService characterService;

    @GetMapping("/character")
    CharacterResponse getCharacter(Authentication auth) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        return CharacterResponse.fromEntity(characterService.get(user));
    }

    @GetMapping("/characters")
    List<CharacterResponse> getCharacters() {
        return characterService.getAll().stream()
            .map(CharacterResponse::fromEntity)
            .collect(Collectors.toList());
    }

    @GetMapping("/characters/{id}")
    CharacterResponse getCharacter(@PathVariable long id) {
        return CharacterResponse.fromEntity(characterService.get(id));
    }

    @DeleteMapping("/characters/{id}")
    void deleteCharacter(Authentication auth, @PathVariable long id) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        characterService.delete(user, id);
    }

    @PostMapping("/characters")
    Long createCharacter(Authentication auth, @RequestBody CreateCharacterRequest request) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        return characterService.create(user, request);
    }
}
