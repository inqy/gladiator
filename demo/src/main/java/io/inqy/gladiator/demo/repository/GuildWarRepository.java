package io.inqy.gladiator.demo.repository;

import io.inqy.gladiator.demo.entity.Guild;
import io.inqy.gladiator.demo.entity.GuildWar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GuildWarRepository extends JpaRepository<GuildWar, Long> {

    List<GuildWar> findAllByAttackerEqualsOrDefenderEquals(Guild attacker, Guild defender);
}
