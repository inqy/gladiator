package io.inqy.gladiator.demo.service;

import io.inqy.gladiator.demo.entity.Character;
import io.inqy.gladiator.demo.entity.*;
import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.exception.EntityNotFoundException;
import io.inqy.gladiator.demo.repository.BattleEventRepository;
import io.inqy.gladiator.demo.repository.BattleRepository;
import io.inqy.gladiator.demo.repository.CharacterRepository;
import io.inqy.gladiator.demo.repository.GuildWarRepository;
import io.inqy.gladiator.demo.service.battle.BattleWrap;
import io.inqy.gladiator.demo.transit.BattleResultResponse;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class BattleService {

    private final BattleRepository repository;
    private final BattleEventRepository eventRepository;
    private final CharacterRepository characterRepository;
    private final GuildWarRepository guildWarRepository;
    private final RatingService ratingService;
    private final CharacterService characterService;
    private final Random random = new Random();

    @Transactional
    public BattleResultResponse attack(SecurityUser attackerUser, Long defenderId) {
        Character attacker = characterRepository.findCharacterByUserLocking(attackerUser).orElseThrow(EntityNotFoundException::new);
        Character defender = characterRepository.findByIdLocking(defenderId).orElseThrow(EntityNotFoundException::new);
        return attack(attacker, defender);
    }

    private BattleResultResponse attack(Character attacker, Character defender) {
        BattleResult result = new BattleResult(attacker, defender).invoke();
        BattleWrap battleWrap = result.getBattleWrap();

        if (result.getResult()) {
            attacker.setExp(attacker.getExp() + 10);
            ratingService.increaseCharacterDailyRating(attacker);
        } else {
            defender.setExp(defender.getExp() + 10);
            ratingService.increaseCharacterDailyRating(defender);
        }

        return BattleResultResponse.fromEntityAndEvents(result.getBattle(), result.getEvents().stream().map(BattleEvent::getMessage).collect(Collectors.toList()));
    }

    public void guildWar(Guild attackers, Guild defenders) {
        GuildWar guildWar = new GuildWar(null, attackers, defenders, true, false, Collections.emptyList());
        guildWarRepository.save(guildWar);

        List<Character> attackerCharacters = characterRepository.findCharactersByGuildEquals(attackers);
        List<Character> defenderCharacters = characterRepository.findCharactersByGuildEquals(defenders);
        List<Match> matches = IntStream.range(0, attackerCharacters.size()).boxed()
            .flatMap(i -> defenderCharacters.stream().skip(i).map(defender -> new Match(attackerCharacters.get(i), defender)))
            .collect(Collectors.toList());
        Map<Character, Long> matchesWon = matches.stream()
            .map(match -> attackInGuildWar(match.getAttacker(), match.getDefender(), guildWar) ? match.getAttacker() : match.getDefender())
            .collect(Collectors.groupingBy(character -> character, Collectors.counting()));
        Long attackerWins = matchesWon.entrySet().stream()
            .filter((entry) -> entry.getKey().getGuild().getId().equals(attackers.getId()))
            .map(Map.Entry::getValue)
            .mapToLong(Long::longValue)
            .sum();
        Long defenderWins = matchesWon.entrySet().stream()
            .filter((entry) -> entry.getKey().getGuild().getId().equals(defenders.getId()))
            .map(Map.Entry::getValue)
            .mapToLong(Long::longValue)
            .sum();
        matchesWon.forEach((character, wins) -> {
            characterService.increaseCharacterExperience(character.getId(), wins * 10);
        });

        guildWar.setFinished(true);
        guildWar.setResult(attackerWins > defenderWins);
        guildWarRepository.save(guildWar);
    }

    private Boolean attackInGuildWar(Character attacker, Character defender, GuildWar war) {
        BattleResult battleResult = new BattleResult(attacker, defender).invoke();
        /* This is dreadfully inefficient */
        List<Battle> battles = new ArrayList<>(war.getBattle());
        battles.add(battleResult.getBattle());
        war.setBattle(battles);
        return battleResult.getResult();
    }

    @Data
    private class Match {
        private final Character attacker;
        private final Character defender;
    }

    @Getter
    @RequiredArgsConstructor
    private class BattleResult {
        private final Character attacker;
        private final Character defender;
        private BattleWrap battleWrap;
        private Boolean result;
        private List<BattleEvent> events;
        private Battle battle;

        BattleResult invoke() {
            battleWrap = new BattleWrap(attacker, defender, random);
            battleWrap.perform();
            result = battleWrap.getResult();
            events = battleWrap.getEvents();

            battle = new Battle(null, result, attacker, defender);
            repository.save(battle);
            events.forEach(event -> {
                event.setBattleId(battle.getId());
                eventRepository.save(event);
            });
            return this;
        }
    }
}
