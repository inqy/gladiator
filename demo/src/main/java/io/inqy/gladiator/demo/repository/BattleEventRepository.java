package io.inqy.gladiator.demo.repository;

import io.inqy.gladiator.demo.entity.BattleEvent;
import io.inqy.gladiator.demo.entity.BattleEventKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleEventRepository extends JpaRepository<BattleEvent, BattleEventKey> {
}
