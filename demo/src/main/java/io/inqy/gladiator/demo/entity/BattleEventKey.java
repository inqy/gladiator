package io.inqy.gladiator.demo.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BattleEventKey implements Serializable {
    Long battleId;
    Integer eventId;
}
