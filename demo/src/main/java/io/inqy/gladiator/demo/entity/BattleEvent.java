package io.inqy.gladiator.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@IdClass(BattleEventKey.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BattleEvent {
    @Id
    Long battleId;
    @Id
    Integer eventId;

    @ManyToOne
    @JoinColumn(name = "battleId", insertable = false, updatable = false)
    Battle battle;

    Long attackerHp;
    Long defenderHp;
    String message;
}

