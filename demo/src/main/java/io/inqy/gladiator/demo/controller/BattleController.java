package io.inqy.gladiator.demo.controller;

import io.inqy.gladiator.demo.entity.security.SecurityUser;
import io.inqy.gladiator.demo.service.BattleService;
import io.inqy.gladiator.demo.transit.BattleResultResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequiredArgsConstructor
public class BattleController {

    private final BattleService battleService;

    @PostMapping("/attack")
    @Transactional
    BattleResultResponse attack(Authentication auth, @RequestBody Long targetCharacterId) {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        return battleService.attack(user, targetCharacterId);
    }
}
